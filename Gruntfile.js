
module.exports = function (grunt) {
  'use strict';

  grunt.util.linefeed = '\n';
  
  var version = '1.0.0';

  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    clean: {
      dist: 'dist'
    },

    concat: {
      options: {
        stripBanners: false
      },
      basejs: {
        src: [
          'js/jquery-2.2.3.min.js',
          'js/jsrender.min.js',
          'js/require.js'
          
        ],
        dest: 'dist/js/base-'+version+'.js'
      },
      basecss: {
          src: [
            'css/font-awesome.min.css',
            'css/bootstrap.min.css'
          ],
          dest: 'dist/css/base-'+version+'.css'
      }
    }
  });
  
  require('load-grunt-tasks')(grunt, { scope: 'devDependencies',
	    pattern: ['grunt-*', '!grunt-sass', '!grunt-contrib-sass'] });
  
  grunt.registerTask('dist-base-js', ['concat:basejs']);
  grunt.registerTask('dist-base-css', ['concat:basecss']);

};
